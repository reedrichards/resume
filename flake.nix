{
  description = "A basic gomod2nix flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
          devshell = import ./shell.nix { inherit pkgs; };
          # make derivation using devshell as a build input
          defaultPackage = pkgs.stdenv.mkDerivation {
            name = "resume";
            src = ./.;
            buildInputs = [
              pkgs.texlive.combined.scheme-full
              pkgs.python3Packages.pygments
              pkgs.gnuplot
            ];
            buildPhase = ''
              mkdir -p $out
              pdflatex -output-directory=$out resume.tex
              cp resume.tex $out
            '';
          };

        in
        {
          devShells.default = devshell;
          packages.default = defaultPackage;
        })
    );
}
